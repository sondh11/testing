package vnGMO_Demo.Iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

import vnGMO_Demo.demo2.Sonar;

public class Controller {
	private static Logger logger = Logger.getLogger(Controller.class.getName());
	SongsOfThe70s songs70s;
	SongsOfThe80s songs80s;

	// NEW Passing in song iterators

	SongIterator iter70sSongs;
	SongIterator iter80sSongs;



	public Controller(SongIterator newSongs70s, SongIterator newSongs80s) {

		iter70sSongs = newSongs70s;
		iter80sSongs = newSongs80s;

	}

	public void showTheSongs() {

		// Because the SongInfo Objects are stored in different
		// collections everything must be handled on an individual
		// basis. This is BAD!

		ArrayList aL70sSongs = songs70s.getBestSongs();

		System.out.println("Songs of the 70s\n");

		for (int i = 0; i < aL70sSongs.size(); i++) {

			SongInfo bestSongs = (SongInfo) aL70sSongs.get(i);

			logger.info(bestSongs.getSongName());
			logger.info(bestSongs.getBandName());
			logger.info(bestSongs.getYearReleased() + "\n");

		}

		SongInfo[] array80sSongs = songs80s.getBestSongs();

		System.out.println("Songs of the 80s\n");

		for (int j = 0; j < array80sSongs.length; j++) {

			SongInfo bestSongs = array80sSongs[j];

			logger.info(bestSongs.getSongName());
			logger.info(bestSongs.getBandName());
			logger.info(bestSongs.getYearReleased() + "\n");

		}

	}

	// Now that I can treat everything as an Iterator it cleans up
	// the code while allowing me to treat all collections as 1

	public void showTheSongs2() {

		logger.info("NEW WAY WITH ITERATOR\n");

		Iterator Songs70s = iter70sSongs.createIterator();
		Iterator Songs80s = iter80sSongs.createIterator();

		logger.info("Songs of the 70s\n");
		displayTheSongs(Songs70s);

		logger.info("Songs of the 80s\n");
		displayTheSongs(Songs80s);

	}

	public void displayTheSongs(Iterator iterator) {

		while (iterator.hasNext()) {

			SongInfo songInfo = (SongInfo) iterator.next();

			logger.info(songInfo.getSongName());
			logger.info(songInfo.getBandName());
			logger.info(songInfo.getYearReleased() + "\n");

		}

	}

}